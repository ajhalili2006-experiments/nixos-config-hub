{ pkgs, ... }:
{
  nix = {
    settings = {
      allowed-users = [ "@wheel" ];
      trusted-users = [ "root" "@wheel" ];
      experimental-features = [ "nix-command" "flakes" ];
    };
  };

  # Does not work without channels.
  programs.command-not-found.enable = false;

  users.mutableUsers = true; # we'll keep this enabled for compat, although I might do this on per-host basis
}
