FROM gitpod/workspace-base

USER root

# Setup nixbuilders
RUN addgroup --system nixbld \
  && adduser gitpod nixbld \
  && for i in $(seq 1 30); do useradd -ms /bin/bash nixbld$i && adduser nixbld$i nixbld; done \
  && mkdir -m 0755 /nix && chown gitpod /nix \
  && mkdir -p /etc/nix && echo 'sandbox = false' > /etc/nix/nix.conf

USER gitpod
ENV USER gitpod
WORKDIR /home/gitpod

# Prepare and install Nix package manager
RUN touch .bash_profile \
 && curl https://nixos.org/releases/nix/nix-2.3.14/install | sh

# Configure stuff
RUN echo '. /home/gitpod/.nix-profile/etc/profile.d/nix.sh' >> /home/gitpod/.bashrc
RUN mkdir -p /home/gitpod/.config/nixpkgs && echo '{ allowUnfree = true; }' >> /home/gitpod/.config/nixpkgs/config.nix

# Install cachix, Git and direnv in one layer on nix
RUN . /home/gitpod/.nix-profile/etc/profile.d/nix.sh \
  && nix-env -iA cachix -f https://cachix.org/api/v1/install \
  && cachix use cachix \
  && nix-env -i git git-lfs \
  && nix-env -i direnv \
  && direnv hook bash >> /home/gitpod/.bashrc

# Install QEMU and other tooling
RUN sudo install-packages qemu qemu-system-x86 linux-image-$(uname -r) libguestfs-tools sshpass netcat

CMD [ "/bin/bash", "-l" ]
