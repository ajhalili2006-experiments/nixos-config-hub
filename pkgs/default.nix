final: prev: {
  railwayapp  = prev.callPackage ./railwayapp {};
  inherit (prev.callPackages ./utils {})
    deploy
    deploy-pages
    git-part-pick
    git-auto-fixup
    git-auto-squash
    nix-explore-closure-size
    nopt
    update
    sman
    strip-clip-path-transforms;
}
