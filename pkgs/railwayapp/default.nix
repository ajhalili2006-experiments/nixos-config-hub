{
  stdenv,
  fetchFromGitHub
}:

stdenv.mkDerivation rec {
    pname = "railway-cli";
    version = "3.0.12";

    src = fetchFromGitHub {
      owner = "railwayapp";
      repo = "cli";
      rev = "v${version}";
      sha256 = "0000000000000000000000000000000000000000000000000000"
    };
};
