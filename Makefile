## default variables for make ##
# Default shell
SHELL := bash
SHARED_DEVENV_SECRET := "7c2ba4e96346734b28de18dae810620d95609a571713a618"

default: build-devenv

## Help and Docs ##
help:
	@echo "Andrei Jiroh's NixOS Image generator and configuration hub"
	@echo
	@echo Building and managing  reproduicible dev envs:
	@echo "    build-devenv   - Compiles and generates the ISO based on the dev environment config"
	@echo "    start-devenv   - Boots a virtual machine using that ISO from build-devenv step"
	@echo "    ssh-devenv     - Connect to the dev environment server."
	@echo
	@echo Managing NixOS install:
	@echo "   update-pkgs   -"
	@echo Utils and docs:
	@echo "    clean          - Remove build artifacts from the source directory."
	@echo "    help           - Prints this help message"
	@echo "    setup-ci       - Install QEMU and Nix-related stuff"

build:
	deploy

build-verbose:
	deploy -- --show-trace
