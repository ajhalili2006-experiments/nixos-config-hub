# ~ajhalili2006's NixOS configuration

Feels like [~jtojnar's nixfiles](https://github.com/jtojnar/nixfiles), but for ~ajhalili2006's devenv setup (especially in Gitpod). Licensed under a mix of MIT and MPL-3.0 (old commits were under GPL-3.0, but later chose to do dual-licensing due to potential legal issues), although ask me for custom licensing agreements if needed.

## Utilities

While the Python scripts from ~jtojnar's repo is licensed under MIT, we do configure things through a Makefile

* `make build` - the `deploy` command wrapped in `nix-shell`
* `make build-verbose` or `make verbose-build` - shows exec traces while running, potentially to help debug issues with your Nixfiles.

## Have questions about this?
