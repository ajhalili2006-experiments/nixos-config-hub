# nixos-config-utils

Most of the scripts here are from [~jtojnar's nix config files repo](https://github.com/jtojnar/nixfiles),
licensed under MIT.
